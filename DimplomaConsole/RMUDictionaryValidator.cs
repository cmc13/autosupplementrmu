﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Text.RegularExpressions;

using WikiMediaApi;
using RMUDLLWrapper;

namespace DimplomaConsole
{
    class RMUDictionaryValidator
    {
        private List<string> dictionary = new List<string>();
        private AccentFinder finder = new AccentFinder();
        
        public RMUDictionaryValidator(string path)
        {
            StreamReader reader = new StreamReader(path);
            while (!reader.EndOfStream)
            {
                string line = reader.ReadLine();
                if(line.Contains("noun"))
                {
                    dictionary.Add(line.Split(new char[1]{' '})[0]);
                }
            }

            reader.Close();
        }

        public int Validate(string outPath)
        {
            StreamWriter writer = new StreamWriter(outPath);

            finder.useSuggestion = false;
            int result = 0;
            foreach (string word in dictionary)
            {
                int accent = finder.FindAccent(word);
                if (accent != 0)
                {
                    ++result;
                    writer.WriteLine(String.Format("{0} {1}", word, accent));
                }
                if (result % 100 == 0)
                {
                    writer.Flush();
                }
            }
            writer.Close();

            return result;
        }

        public int ValidateAccents(string inPath, string outPath)
        {
            StreamReader reader = new StreamReader(inPath);
            StreamWriter writer = new StreamWriter(outPath);
            int result = 0;
            char[] spaceSeparator = new char[1] { ' ' };
            char[] eqSeparator = new char[1] { '=' };
            RMUnit rmu = new RMUnit();

            Regex regex = new Regex("stress=([0-9]*)");

            while (!reader.EndOfStream)
            {
                string line = reader.ReadLine();
                string[] splitted = line.Split(spaceSeparator);
                
                string word = splitted[0];

                if (word.Length < 4)
                {
                    continue;
                }

                string accent = splitted[1];
                string xml = "";
                try
                {
                    xml = rmu.Analyse(word);
                }
                catch (Exception)
                {
                    continue;
                }

                string rmuAccent = "( ";
           
                bool yes = false;
                foreach (Match m in regex.Matches(xml))
                {
                    string s = m.Value.Split(eqSeparator)[1];
                    if (rmuAccent.Contains(s))
                    {
                        continue;
                    }
                    rmuAccent += s + " ";
                    if (s == accent)
                    {
                        yes = true;
                        break;
                    }
                }
                if (!yes)
                {
                    rmuAccent += ")";
                    writer.WriteLine(word + " " + rmuAccent + " " + accent);
                    writer.Flush();
                    ++result;
                }
            }

            rmu.Free();
            writer.Close();
            reader.Close();

            return result;
        }

        public int GetWordsCount()
        {
            return dictionary.Count;
        }
    }
}
