﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.Web;
using System.IO;
using System.Xml;
using System.Text.RegularExpressions;
using Word = Microsoft.Office.Interop.Word;

using RMUDLLWrapper;
using WikiMediaApi;

namespace DimplomaConsole
{
    class Program
    {
        static void Main(string[] args)
        {
            String[] phrases = new String[] { 
                "Вулканы", "Озера", "Города России", "Великие люди",
                "Столицы", "Страны", "Президенты США",
                "Пещера", "Эпохи", "Короли",
                "Герои Великой Отечественной Войны", "Университеты", "Знаменитые корабли",
                "Города Франции", "Религия", "Империя"
            };

            ProperNounFinder finder = new ProperNounFinder();

            foreach (String phrase in phrases)
            {
                finder.FindWords(phrase);
            }
            Console.WriteLine("DONE");
            Console.ReadKey();
        }
    }
}
