﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.IO;
using System.Text.RegularExpressions;

using WikiMediaApi;
using RMUDLLWrapper;

namespace DimplomaConsole
{
    class ProperNounFinder
    {
        [Flags]
        private enum States
        {
            IDLE,
            SENT_BEGIN,
            SENT_INSIDE,
            SENT_OUTSIDE,
            WORD,
            CAPITAL_WORD,
            ABBREVIATION,
            PROPER_NAME
        };

        private char[] VALID_CHARACTERS = Encoding.Unicode.GetChars(Encoding.Unicode.GetBytes("йцукенгшщзхъфывапролджэячсмитьбюЙЦУКЕНГШЩЗХФЫВАПРОЛДЖЭЯЧСМИТЬБЮЁё"));
        private char[] UPPER_CHARACTERS = Encoding.Unicode.GetChars(Encoding.Unicode.GetBytes("ЙЦУКЕНГШЩЗХФЫВАПРОЛДЖЭЯЧСМИТЬБЮЁ"));
        private const char ACCENT_CHARACTER = '\u0301';
        private char[] ENDS_OF_SENTENCES = Encoding.Unicode.GetChars(Encoding.Unicode.GetBytes(".!?|="));
        private const string END_OF_SENTANCE = ".";
        private char[] SEPARATORS = Encoding.Unicode.GetChars(Encoding.Unicode.GetBytes(" ();,-—"));
        private const char END_OF_ARTICLE = '\0';

        private States state = States.IDLE;

        private WikiMediaApiWrapper wiki = new WikiMediaApiWrapper();
        private RMUnit rmu = new RMUnit();
        private AccentFinder accentFinder = new AccentFinder();

        private List<string> words = new List<string>();
        private Dictionary<string, int> findedAccents = new Dictionary<string, int>();
        
        private char[] article;
        private int cursor;
        private char current = END_OF_ARTICLE;

        private StreamWriter file;
        private int writtenWordsCount = 0;

        private Dictionary<string, int> foundedAccents = new Dictionary<string,int>();

        public List<string> FindWords(string searchPhrase)
        {
            wiki.limit = 50;
            XmlDocument xml = wiki.findArticles(searchPhrase);
            XmlNode apiNode = xml.GetElementsByTagName("api")[0];
            XmlNode queryNode = xml.GetElementsByTagName("query")[0];
            XmlDocument article;

            words.Clear();
            file = new StreamWriter(searchPhrase + ".txt");
            writtenWordsCount = 0;
            foreach (XmlNode node in queryNode.SelectSingleNode("search").ChildNodes)
            {
                string title = node.Attributes["title"].Value;
                article = wiki.getArticlesByTitle(title);
                proccessArticle(article);
                //break;
            }
            file.Close();
            return words;
        }

        private void proccessArticle(XmlDocument articleXml){
            Console.WriteLine("Article processing begin");
            if (!init(articleXml))
            {
                return;
            }
            bool endOfArticle = false;
            StringBuilder properNoun = new StringBuilder();
            while (!endOfArticle)
            {
               // Console.Write(state);
                switch (state)
                {
                    case States.IDLE:
                        GetNextChar();
                        if (IsUpper(current))
                        {
                            state = States.SENT_BEGIN;
                        }
                        else if (IsAlphaChar(current))
                        {
                            state = States.WORD;
                        }
                        else
                        {
                            state = States.SENT_OUTSIDE;
                        }
                    break;

                    case States.SENT_BEGIN:
                        GetNextChar();
                        if (IsSentsDelimiter(current))
                        {
                            state = States.SENT_OUTSIDE;
                        }
                        else if (IsAlphaChar(current))
                        {
                            state = States.SENT_BEGIN;
                        }
                        else
                        {
                            state = States.SENT_INSIDE;
                        }
                    break;

                    case States.SENT_INSIDE:
                        GetNextChar();
                        if (IsUpper(current))
                        {
                            state = States.CAPITAL_WORD;
                        }
                        else if (IsSentsDelimiter(current))
                        {
                            state = States.SENT_OUTSIDE;
                        }
                        else if (IsAlphaChar(current))
                        {
                            state = States.WORD;
                        } 
                        else
                        {
                            state = States.SENT_INSIDE;
                            //Console.WriteLine(String.Format("Char:[{0}][{1}]", current, (int)current));
                        }
                    break;

                    case States.SENT_OUTSIDE:
                        GetNextChar();
                        if (IsAlphaChar(current))
                        {
                            state = States.SENT_BEGIN;
                        }
                        else if (current == END_OF_ARTICLE)
                        {
                            endOfArticle = true;
                        }
                        else
                        {
                            state = States.SENT_OUTSIDE;
                        }
                    break;

                    case States.WORD:
                        GetNextChar();
                        if (IsAlphaChar(current))
                        {
                            state = States.WORD;
                        }
                        else if(IsSentsDelimiter(current))
                        {
                            state = States.SENT_OUTSIDE;
                        }
                        else
                        {
                            state = States.SENT_INSIDE;
                        }
                    break;
                    
                    case States.CAPITAL_WORD:
                        properNoun.Clear();
                        properNoun.Append(current);
                        GetNextChar();
                        if (IsUpper(current))
                        {
                            state = States.ABBREVIATION;
                        }
                        else if (IsAlphaChar(current))
                        {
                            properNoun.Append(current);
                            state = States.PROPER_NAME;
                        }
                        else if(current == '.')
                        {
                            state = States.SENT_INSIDE;
                        }
                        else if (IsSentsDelimiter(current))
                        {
                            state = States.SENT_OUTSIDE;
                        }
                        else
                        {
                            state = States.SENT_INSIDE;
                        }
                    break;

                    case States.ABBREVIATION:
                        GetNextChar();
                        if (IsAlphaChar(current))
                        {
                            state = States.ABBREVIATION;
                        }
                        else if (IsSentsDelimiter(current))
                        {
                            state = States.SENT_OUTSIDE;
                        }
                        else
                        {
                            state = States.SENT_INSIDE;
                        }
                    break;

                    case States.PROPER_NAME:
                        GetNextChar();
                        if (IsUpper(current))
                        {
                            state = States.ABBREVIATION;
                        }
                        else if(IsSentsDelimiter(current))
                        {
                            state = States.SENT_OUTSIDE;
                            OnProperNounFound(properNoun);
                        }
                        else if (IsAlphaChar(current))
                        {
                            properNoun.Append(current);
                            state = States.PROPER_NAME;
                        }
                        else
                        {
                            state = States.SENT_INSIDE;
                            OnProperNounFound(properNoun);
                        }
                    break;
                }
            }
            Console.WriteLine("Article process end");
        }

        private void OnProperNounFound(StringBuilder properNoun)
        {
            string word = properNoun.ToString();
            properNoun.Clear();
            
            if(foundedAccents.ContainsKey(word))
            {
                return;
            }

            if (rmu.Analyse(word).Contains("unrecognized"))
            {
                Console.WriteLine(word);
                int accent = accentFinder.FindAccent(word);
                foundedAccents.Add(word, accent);
                file.WriteLine(String.Format("{0} {1}", word, accent));
                ++writtenWordsCount;
                if (writtenWordsCount % 100 == 0)
                {
                    file.Flush();
                }
            }
        }

        private bool init(XmlDocument articleXml)
        {
            string text = null;
            try
            {
                text = articleXml.GetElementsByTagName("pages")[0]
                            .SelectSingleNode("page")
                            .SelectSingleNode("revisions")
                            .SelectSingleNode("rev")
                            .InnerText;
            }
            catch (NullReferenceException e)
            {
                Console.WriteLine(e.StackTrace);
                return false;
            }
            article = Encoding.Unicode.GetChars(Encoding.Unicode.GetBytes(text));
            cursor = 0;
            state = States.IDLE;
            return true;
        }

        private void GetNextChar()
        {
            if (cursor >= article.Length)
            {
                current = END_OF_ARTICLE;
            }
            else
            {
                current = article[cursor++];
            }

            return;
        }

        private bool IsSentsDelimiter(char c)
        {
            return END_OF_SENTANCE.Contains(c) || c == END_OF_ARTICLE;
        }

        private bool IsUpper(char c)
        {
            return UPPER_CHARACTERS.Contains(c);
        }

        private bool IsAlphaChar(char c)
        {
            return VALID_CHARACTERS.Contains(c);
        }

    }
}
