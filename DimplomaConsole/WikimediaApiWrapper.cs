﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.Web;
using System.IO;
using System.Xml;

namespace WikiMediaApi
{
    class WikiMediaApiWrapper
    {
        const String USER_AGENT = "Footbler";
        const String URL_SEARCH = "http://ru.wikipedia.org/w/api.php?action=query&srlimit={0}&list=search&format=xml&srsearch={1}";
        const String URL_ARTICLE = "http://ru.wikipedia.org/w/api.php?action=query&prop=revisions&rvlimit=1&rvprop=content&format=xml&titles={0}";
        public const int DEFAULT_LIMIT = 5;
        public const int MAX_LIMIT = 50;

        private string userAgent;

        public int limit
        {
            get;
            set;
        }
        
        public WikiMediaApiWrapper(string userAgent = USER_AGENT)
        {
            this.userAgent = userAgent;
            limit = DEFAULT_LIMIT;
        }

        public XmlDocument findArticles(string text)
        {
            return sendRequest(String.Format(URL_SEARCH, limit, HttpUtility.UrlEncode(text)));
        }

        public XmlDocument getArticlesByTitle(string title)
        {
            Console.WriteLine(String.Format("Loading article: {0}", title));
            return sendRequest(String.Format(URL_ARTICLE, HttpUtility.UrlEncode(title)));
        }


        private XmlDocument sendRequest(string url)
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            request.UserAgent = userAgent;
            HttpWebResponse response;
            try
            {
                response = request.GetResponse() as HttpWebResponse;
            }
            catch (WebException)
            {
                return null;
            }
            Stream resStream = response.GetResponseStream();
            int count;
            string tempString;
            byte[] buf = new byte[8192];
            StringBuilder sb = new StringBuilder();

            do
            {
                count = resStream.Read(buf, 0, buf.Length);
                if (count != 0)
                {
                    tempString = Encoding.UTF8.GetString(buf, 0, count);
                    sb.Append(tempString);
                }
            } while (count > 0);

            XmlDocument xml = new XmlDocument();
            xml.LoadXml(sb.ToString());

            return xml;
        }
    }
}
