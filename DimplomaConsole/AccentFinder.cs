﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

namespace WikiMediaApi
{
    class AccentFinder
    {
        public char[] VOWELS = Encoding.Unicode.GetChars(Encoding.Unicode.GetBytes("аАуУоОыЫиИэЭяЯюЮеЕ"));
        public const char ACCENT_CHARACTER = '\u0301';

        public bool useSuggestion {get; set;}
        public bool isSuggestionUsed
        {
            get;
            private set;
        }
        public string suggestion 
        {
            get;
            private set;
        }

        private WikiMediaApiWrapper wiki = new WikiMediaApiWrapper();

        public AccentFinder()
        {
            useSuggestion = false;
            isSuggestionUsed = false;
            suggestion = null;
            wiki.limit = 3;
        }

        public int FindAccent(string word)
        {
            string lower = word.ToLower();
            if (lower.Contains("ё"))
            {
                return lower.IndexOf("ё") + 1;
            }

            List<char[]> mayBeAccents = new List<char[]>();
            char[] wordChars = Encoding.Unicode.GetChars(Encoding.Unicode.GetBytes(lower));
            char[] buffer = new char[wordChars.Length + 1];
            int index = 0;
            foreach (char c in wordChars)
            {
                buffer[index] = c;
                if (VOWELS.Contains(c))
                {
                    char[] mayBeAccent = new char[wordChars.Length + 1];
                    Array.Copy(wordChars, 0, mayBeAccent, 0, index + 1);
                    mayBeAccent[index + 1] = ACCENT_CHARACTER;
                    Array.Copy(wordChars, index + 1, mayBeAccent, index + 2, wordChars.Length - (index + 1));
                    mayBeAccents.Add(mayBeAccent);
                }
                ++index;
            }

            if (mayBeAccents.Count == 0)
            {
                return 0;
            }

            if (mayBeAccents.Count == 1)
            {
                return Array.IndexOf(mayBeAccents.ElementAt(0), ACCENT_CHARACTER);
            }

            List<string> mayBeAccentsStrings = new List<string>();
            foreach(char[] m in mayBeAccents){
                string s = Encoding.Unicode.GetString(Encoding.Unicode.GetBytes(m));
                mayBeAccentsStrings.Add(s);
                //Console.WriteLine(s);
            }

            XmlDocument xml = wiki.findArticles(word);
            XmlNode apiNode = xml.GetElementsByTagName("api")[0];
            XmlNode queryNode = xml.GetElementsByTagName("query")[0];
            XmlDocument article;
            XmlNodeList searchResult = queryNode.SelectSingleNode("search").ChildNodes;
            if(searchResult.Count == 0 && useSuggestion)
            {
                XmlAttribute suggestionAttribute = queryNode.SelectSingleNode("searchinfo").Attributes["suggestion"];
                if(suggestionAttribute != null){
                    suggestion = suggestionAttribute.Value;
                    if (suggestion != null)
                    {
                        isSuggestionUsed = true;
                        return FindAccent(suggestion);
                    }

                }

                return 0;
            }
            foreach (XmlNode node in searchResult)
            {
                string title = node.Attributes["title"].Value;
               // Console.Write("Load article: " + title + "...");
                article = wiki.getArticlesByTitle(title);
                string text = article.GetElementsByTagName("pages")[0]
                            .SelectSingleNode("page")
                            .SelectSingleNode("revisions")
                            .SelectSingleNode("rev")
                            .InnerText;

                text = text.ToLower();
                char[] textChars = Encoding.Unicode.GetChars(Encoding.Unicode.GetBytes(text));

                foreach (string mayBeAccent in mayBeAccentsStrings)
                {
                    if (text.Contains(mayBeAccent))
                    {
                        Console.WriteLine(mayBeAccent);
                        return mayBeAccent.IndexOf(ACCENT_CHARACTER);
                    }
                }

            }

            return 0;

        }
    }
}
