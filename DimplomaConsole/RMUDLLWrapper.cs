﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;
using System.Runtime.ExceptionServices;

namespace RMUDLLWrapper
{
    class RMUnit
    {
        public RMUnit()
        {
            // load RMU library
            _RMUHandle = IntPtr.Zero;
            if (RMUDLLWrapper.RMUInit(ref _RMUHandle) != RMUDLLWrapper.RMU_OK)
                throw new Exception("Ошибка инициализации библиотеки");
        }
        public void Free()
        {
            RMUDLLWrapper.RMUFree(_RMUHandle);
            _RMUHandle = IntPtr.Zero;
        }

        [HandleProcessCorruptedStateExceptions]
        unsafe public string Analyse(string word)
        {
            byte[] encWord = System.Text.Encoding.GetEncoding(1251).GetBytes(word);
            IntPtr hAnsw = IntPtr.Zero;
            try
            {
                try
                {
                    if (RMUDLLWrapper.RMUGetAnswer(_RMUHandle, encWord, ref hAnsw) != RMUDLLWrapper.RMU_OK)
                        throw new Exception("Ошибка анализа слова " + word);
                }
                catch (AccessViolationException)
                {
                    return "";
                }
                catch (Exception)
                {
                    return "";
                }
                byte* pByte = null;
                if (RMUDLLWrapper.RMUGetXML(hAnsw, ref pByte) != RMUDLLWrapper.RMU_OK)
                    throw new Exception("Unexpected error");
                int len = 0; byte* pB = pByte;
                while (*pB++ != 0) len++;
                byte[] answerText = new byte[len];
                pB = pByte;
                for (int i = 0; i < len; i++) answerText[i] = pB[i];
                string xml = Encoding.GetEncoding(1251).GetString(answerText);
                return xml;
            }
            catch (Exception)
            {
                return "";
            }
            finally
            {
                if (hAnsw != IntPtr.Zero)
                    RMUDLLWrapper.RMUFreeAnswer(hAnsw);
            }
            
        }
        #region private members
        private IntPtr _RMUHandle;
        #endregion private members
    }

    static class RMUDLLWrapper
    {
        internal const int RMU_OK = 0;
        const string DLLName = "RMUDLL.dll";
        [DllImport(DLLName, CallingConvention=CallingConvention.Cdecl, SetLastError = true)]
        internal static extern unsafe int RMUInit(ref IntPtr hRMU);
        [DllImport(DLLName, CallingConvention = CallingConvention.Cdecl, SetLastError = true)]
        internal static extern unsafe int RMUFree(IntPtr hRMU);
        [DllImport(DLLName, CallingConvention = CallingConvention.Cdecl, SetLastError = true)]
        internal static extern unsafe int RMUGetAnswer(IntPtr hRMU, byte[] word, ref IntPtr hAnsw);
        [DllImport(DLLName, CallingConvention = CallingConvention.Cdecl, SetLastError = true)]
        internal static extern unsafe int RMUGetXML(IntPtr hAnsw, ref byte * pByte);
        [DllImport(DLLName, CallingConvention = CallingConvention.Cdecl, SetLastError = true)]
        internal static extern unsafe int RMUFreeAnswer(IntPtr hAnsw);
    }
}
